﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SinglePagePractise.Controllers {
	public class MainController : Controller {
		// GET: Main
		public ActionResult Index() {
			return View();
		}

		public async Task<ActionResult> SendData(string text) {
			MailAddress from = new MailAddress("mukhamedya.n@gmail.com", "Niyaz");
            MailAddress to = new MailAddress("mukhamedya.n@gmail.com");
            MailMessage m = new MailMessage(from, to);
            m.Subject = "Test";
            m.Body = text;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new NetworkCredential("mukhamedya.n@gmail.com", "...");
			smtp.EnableSsl = true;
			await smtp.SendMailAsync(m);
            Console.WriteLine("Письмо отправлено");
			return View("Index");
		}
	}
}